About Matches:

Matches is a web application that combines the aspects of music applications, like Spotify and Pandora, with social media. The main reason it is called 'Matches' is because based on the music you like and the singers you follow, it can match you to other users with similar preferences.

It utilizes the Spotify and Facebook API. We also use the Django framework and Postgres(Not as of yet, still on SQLite3) to help manage the website on the backend. The frontend consists of jQuery/HTML/CSS. 

This project is still currently being worked on...

Note: This project requires you have Django and python already installed.
Launching local server:

1. Go to 'matches' folder and you should find a 'manage.py' file

2. In the command line: $ python manage.py runserver

3. Then in your preferred browser, go to 'localhost:8000' and it should lead you to the page