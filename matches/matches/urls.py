from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'', include('matches_app.urls')),
    url(r'^$', 'matches_app.views.home'),
)
