# General functions like registration and login are here

import base64
import datetime
import json
import re
import requests
import sys
import urllib

from django.shortcuts import render, redirect
from django.core.exceptions import ObjectDoesNotExist
# Decorator to use built-in authentication system
from django.contrib.auth.decorators import login_required
# Used to create and manually log in a user
from django.contrib.auth.models import User
from django.contrib.auth import login, authenticate
from matches_app.models import *
from matches_app.forms import *

from django.shortcuts import render, redirect, get_object_or_404
from django.core.urlresolvers import reverse
from django.db import transaction
from django.http import HttpResponse, Http404
from mimetypes import guess_type
from django.core.mail import send_mail
from django.contrib.auth.tokens import default_token_generator
from django.contrib.auth.tokens import PasswordResetTokenGenerator

context = {}

@login_required
def authorize(request):
    url = 'https://accounts.spotify.com/api/token'
    client_id = "fb72a4508773472bbbcfbaf1b513d3e6"
    client_secret = "19f7b2f693dc439caa2f16938c6e508c"
    payload = client_id + ":" + client_secret
    encode = base64.b64encode(payload)
    headers = { 'Authorization': 'Basic ' + encode}
    data = {'grant_type': 'client_credentials'}
    r = requests.post(url, data=data, headers=headers)
    return HttpResponse(json.dumps(r.json()), content_type="application/json")

# Render home page
@login_required
def home(request):
    personal = get_object_or_404(Personal, user=request.user)
    print request.user
    return render(request, 'home.html', {'personal' : personal})

# Render music search page
@login_required
def music_search(request):
    personal = get_object_or_404(Personal, user=request.user)
    context = {'personal' : personal}
    return render(request, 'music-dashboard.html', context)

# Return rendered album template html
@login_required
def album_template(request):
    personal = get_object_or_404(Personal, user=request.user)
    context = { 
        'album_name' : request.POST['album_name'],
        'album_num' : request.POST['album_num'],
        'album_img' : request.POST['album_img'],
        'album_id' : request.POST['album_id'],
        'album_artist' : request.POST['album_artist'],
        'artist_id' : request.POST['artist_id'],
        'col_size' : request.POST['col_size'],
        'spotify_url' : request.POST['spotify_url']
    }
        
    if personal.favorite_songs.all().filter(song_id=request.POST['album_id']).exists():
        context['is_liked'] = True
    else:
        context['is_liked'] = False

    if personal.favorite_singers.all().filter(singer_id=request.POST['artist_id']).exists():
        context['artist_followed'] = True
    else :
        context['artist_unfollowed'] = False

    return render(request, 'album-template.html', context)

@login_required
def match(request):  
    personal = get_object_or_404(Personal, user=request.user)
    context['personal'] = personal
    thisuser = Personal.objects.filter(user=request.user)[0]
    context['printname'] = thisuser
    context['favorite_songs'] = [song.name for song in personal.favorite_songs.all()]
    for p in Personal.objects.all():
        p.score = 0
        p.save()
    for song in personal.favorite_songs.all():
        n = len(personal.favorite_songs.all())
        if n > 40:
            singles = 3
        if n <= 40:
            singles = 5
        if n <=20:
            singles = 10
        if n <= 10:
            singles = 20

        userslikethissong = Personal.objects.exclude(user=request.user).filter(favorite_songs__id=song.id)
        for commonuser in userslikethissong:
            if commonuser.score + singles <= 100:
                commonuser.score = commonuser.score + singles
                commonuser.save()

    context['userprofile'] = Personal.objects.exclude(user=request.user).order_by('-score')
    return render(request, 'match.html',context)  

@login_required
def get_photo(request, id):
    profile = get_object_or_404(Personal, id=id)
    if not profile.picture:
        raise Http404
    content_type = guess_type(profile.picture.name)
    return HttpResponse(profile.picture, content_type=content_type)

@transaction.atomic
def register(request):
    # Just display the registration form if this is a GET request
    if request.method == 'GET':
        context['form']=RegistrationForm()
        return render(request, 'register.html', context)
    form = RegistrationForm(request.POST)
    context['form']=form

    if not form.is_valid():
        return render(request, 'register.html', context)
    # Creates the new user from the valid form data
    new_user = User.objects.create_user(username=request.POST['username'], \
                                        password=request.POST['password1'],
                                        email=form.cleaned_data['email'])
    new_user.is_active = False
    new_user.save()

    personal = Personal(user=new_user)
    personal.save()

    token = default_token_generator.make_token(new_user)
    # Logs in the new user and redirects to his/her todo list
    email_body = """
Welcome to the Matches.  Please click the link below to
verify your email address and complete the registration of your account:

  http://%s%s
""" % (request.get_host(), 
       reverse('confirm', args=(new_user.username, token)))
    send_mail(subject="Verify your email address",
              message= email_body,
              from_email="service@match.com",
              recipient_list=[new_user.email])

    context['email'] = form.cleaned_data['email']
    return render(request, 'needs-confirmation.html', context)

@transaction.atomic
def fbregister(request):
    # Just display the registration form if this is a GET request
    if request.method == 'GET':
        context['form']=FBRegistrationForm()
        return render(request, 'fbregister.html', context)
    form = FBRegistrationForm(request.POST)
    context['form']=form

    if not form.is_valid():
        return render(request, 'fbregister.html', context)
    # Creates the new user from the valid form data
    new_user = User.objects.create_user(username=request.POST['username'], \
                                        password=request.POST['password'])
    # new_user.is_active = False
    new_user.is_active = True
    usname = new_user
    print usname
    personalnew = Personal(user=usname)
    print personalnew
    personalnew.save()
    new_user.save()
    new_user.set_password(123)
    
    # User.backend = 'django.contrib.auth.backends.ModelBackend'
    new_user = authenticate(username=request.POST['username'], \
                                        password=request.POST['password'])
    login(request, new_user)
    personal = get_object_or_404(Personal, user=usname)
    print personal
    print request.user
    return render(request, 'home.html', {'personal' : personal})
    

@transaction.atomic
def confirm_registration(request, username, token):
    user = get_object_or_404(User, username=username)
    # Send 404 error if token is invalid
    if not default_token_generator.check_token(user, token):
        raise Http404
    # Otherwise token was valid, activate the user.
    user.is_active = True
    user.save()
    return render(request, 'confirmed.html', {})



@transaction.atomic
def confirm_password(request, username, token):
    context['username']=username
    return render(request, 'reset-password.html', context)

@transaction.commit_on_success
def recoverpassword(request):
    errors=[]
    success=[]
    if not 'username' in request.POST:     
        errors.append('The uesrname is needed.')
        return render(request, 'recover-password.html', {})    
    if 'username' in request.POST:
        requestuser = User.objects.filter(username=request.POST['username'])[0]
        emailaddress = requestuser.email
        nameofuser = request.POST['username']
        p0 = PasswordResetTokenGenerator()
        token = p0.make_token(requestuser)
        email_body = """
    Welcome to the Matches.  You could click the link below to
    verify your email address and recover your password:
      http://%s%s
    """ % (request.get_host(), 
           reverse('confirmpossword', args=(nameofuser, token)))
        send_mail(subject="Verify your email address",
                message= email_body,
                from_email="service@matches.com",
                recipient_list=[emailaddress])
        context = {'errors' : errors, 'emailaddress': emailaddress, 'username':nameofuser }
        return render(request, 'confirm-password.html', context)

@transaction.commit_on_success
def resetpassword(request):
    usernamenow = request.POST.get('requestusername',False)
    user = User.objects.filter(username=usernamenow)[0]
    success = []
    error = []
    if request.POST['password1'] != request.POST['password2']:
    	context['error'] = 'Input password is not the same'
    	return render(request, 'reset-password.html', context)
    else:
    	form = EditPasswordForm(user, request.POST)
    	context['form'] = form
    	user.set_password(request.POST['password1'])
    	user.save()
    	error = []
    if request.POST['password1']:
    	print "Password Saved"
    	context['success'] = 'Successfully changed password'
    return render(request, 'reset-password.html', context)

@transaction.commit_on_success
def editpassword(request):
    user=request.user
    success=[]
    error=[]
    if request.method == 'GET':
        context['form'] = EditPasswordForm(user)
        return render(request, 'edit-password.html', context)    
    form = EditPasswordForm(user, request.POST)
    context['form'] = form

    user.set_password(request.POST['newpassword'])
    user.save()
    print "Password Saved"
    context['success'] = 'Successfully changed password'
    return render(request, 'edit-password.html', context)
    
@login_required
@transaction.atomic
def edit_user(request,id):
    entry_to_edit = get_object_or_404(Personal, user=request.user)
    if request.method == 'GET':
        form = ProfileForm(instance=entry_to_edit)  # Creates form from the 
        context = {'form':form, 'id':id, 'profile' : entry_to_edit} # existing entry.
        return render(request, 'edit-user.html', context)

    # if method is POST, get form data to update the model
    form = ProfileForm(request.POST, request.FILES, instance=entry_to_edit)

    if not form.is_valid():
        context = {'form':form, 'id':id, 'profile' : entry_to_edit} 
        return render(request, 'edit-user.html', context)

    form.save()
    return redirect(reverse('profile', kwargs={'id' : id}))

