from django.db import models
import datetime
from django.contrib.auth.models import User

class Singer(models.Model):
	name = models.CharField(max_length=100)
	singer_id = models.CharField(max_length=100)

	def __unicode__(self):
		return self.name

class Song(models.Model):
	name = models.CharField(max_length=100)
	song_id = models.CharField(max_length=100)
	singer = models.ForeignKey(Singer)

	def __unicode__(self):
		return self.name

class Greeting(models.Model):
	user = models.ForeignKey(User)
	message = models.CharField(max_length=100)

	def __unicode__(self):
		return str(self.id)

class Personal(models.Model):
	user = models.OneToOneField(User)
	intro = models.CharField(max_length=42, blank=True)
	create_time = models.DateTimeField(auto_now_add=True)
	friends = models.ManyToManyField('self',symmetrical=False)
	favorite_singers = models.ManyToManyField(Singer)
	favorite_songs = models.ManyToManyField(Song,related_name='favoritesong', symmetrical=False)
	emotion_song = models.OneToOneField(Song, blank=True, null=True)
	picture = models.ImageField(upload_to="profile_pics", blank=True, default='/media/profile_pics/default.jpg')
	birthday = models.DateField(max_length=42, blank=True, null=True)
	location = models.CharField(max_length=42, blank=True)
	greetings = models.ManyToManyField(Greeting, symmetrical=False, blank=True, null=True)
	score = models.IntegerField(max_length=8, default=0)

	def __unicode__(self):
		return self.user.username
		
	@staticmethod
	def get_profile(user):
		if Personal.objects.filter(user=user):
			return Personal.objects.filter(user=user)[0]
		else:
			return Personal.objects.filter(user=user)

class Tag(models.Model):
	tag_name = models.CharField(max_length=42)

	def __unicode__(self):
		return self.tag_name

class Comment(models.Model):
	who_commented = models.ForeignKey(Personal)
	comment = models.CharField(max_length=200)
	commented_at = models.DateTimeField(auto_now_add=True)
	replied_comment = models.ForeignKey('self', related_name="test", blank=True, null=True)

	def __unicode__(self):
		return str(self.id)

class Forum(models.Model):
	who_posted = models.ForeignKey(Personal)
	posted_at = models.DateTimeField(auto_now_add=True)
	title = models.CharField(max_length=100)
	info = models.CharField(max_length=400)
	tags = models.ManyToManyField(Tag, symmetrical=False, blank=True, null=True)
	comments = models.ManyToManyField(Comment, symmetrical=False, blank=True, null=True)

	def __unicode__(self):
		return self.title	



