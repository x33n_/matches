# All favoriting functionality are here
# Favoriting/unfavoriting songs
# Favoriting/unfavoriting singers

import json
import sys
import urllib
import views

from django.core.urlresolvers import reverse
from django.core.exceptions import ObjectDoesNotExist
from django.db import transaction
from django.shortcuts import render, redirect, get_object_or_404

# Decorator to use built-in authentication system
from django.contrib.auth.decorators import login_required

# Used to create and manually log in a user
from django.contrib.auth.models import User

# To manually create HttpResponses or raise Http404 exception
from django.http import HttpResponse, Http404

from matches_app.models import *
from matches_app.forms import *

# In order to minimize data in the database, we will only
# add music in the database if a user has favorited it
# Adds song to user's favorite songs
@login_required
def favorited_song(request):
	personal = get_object_or_404(Personal, user=request.user)
	# Create new singer if DNE & add to Database
	try:
		singer = get_object_or_404(Singer, singer_id=request.POST['singer_id'])
	except Http404:
		singer = Singer(name=request.POST['singer'],
			   			singer_id=request.POST['singer_id'])
		singer.save()
		
	try:
		song = get_object_or_404(Song, song_id=request.POST['song_id'])
	except Http404:
		song = Song(singer=singer, name=request.POST['song'], 
					song_id=request.POST['song_id'])
		song.save()

	if (not personal.favorite_songs.filter(song_id=song.song_id).exists()):
		personal.favorite_songs.add(song)

	return HttpResponse("OK")

# Remove song from user's favorite songs
@login_required
def unfavorited_song(request):
	personal = get_object_or_404(Personal, user=request.user)
	try:
		song = get_object_or_404(Song, song_id=request.POST['song_id'])
	except Http404:
		return HttpResponse("OK")

	personal.favorite_songs.remove(song)

	return HttpResponse("OK")

# Return user's list of favorite songs
@login_required
def get_favorited_songs(request):
	personal = get_object_or_404(Personal, id=request.POST['user_id'])
	data = {}
	data['songs'] = [song.song_id for song in personal.favorite_songs.all()]
	return HttpResponse(json.dumps(data), content_type="application/json")

# Adds singer to user's singer following
@login_required
def favorited_singer(request):
	print >>sys.stderr, 'favorited singer'
	personal = get_object_or_404(Personal, user=request.user)

	try:
		singer = get_object_or_404(Singer, singer_id=request.POST['singer_id'])
	except Http404:
		singer = Singer(name=request.POST['singer'],
			   			singer_id=request.POST['singer_id'])
		singer.save()

	personal.favorite_singers.add(singer); 

	return HttpResponse("OK")

# Removes singer to user's singer following
@login_required
def unfavorited_singer(request):
	personal = get_object_or_404(Personal, user=request.user)
	singer = get_object_or_404(Singer, singer_id=request.POST['singer_id'])
	personal.favorite_singers.remove(singer)

	return HttpResponse("OK")

# Get list of user's favorite singers
@login_required
def get_favorited_singers(request):
	personal = get_object_or_404(Personal, id=request.POST['user_id'])
	data = {}
	data['favorite_singers'] = [(singer.name, singer.singer_id) for singer in personal.favorite_singers.all()]
	return HttpResponse(json.dumps(data), content_type="application/json")

# Get the 20 most recent songs users favorited
@login_required
def get_recent_favorites(request):
	all_favs = []
	for per in Personal.objects.all():
		songs = per.favorite_songs.all().values("song_id","id")
		for song in songs:
			all_favs.append((song["id"], per.user.username, song['song_id']))

	all_favs.sort(reverse=True)
	print >>sys.stderr, all_favs
	data = {}
	l = len(all_favs)
	if (l >= 20):
		all_favs = all_favs[l-20: l]
		
	return HttpResponse(json.dumps(all_favs), content_type="application/json")

# Return rendered template html for recent favorites
@login_required
def recent_favs_template(request):
	user = get_object_or_404(User, username=request.POST['username'])
	personal = get_object_or_404(Personal, user=user)
	context = {'username' : request.POST['username'],
				'img_src' : request.POST['img_src'],
				'artist' : request.POST['artist'],
				'album' : request.POST['album'],
				'profile_img' : personal.picture.url}
	return render(request, 'recent-fav-template.html', context)

