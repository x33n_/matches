from django import forms
from django.contrib.auth.forms import AuthenticationForm, PasswordResetForm, SetPasswordForm
from matches_app.models import *
from django.contrib.auth.models import User
from django.forms import ModelForm

class RegistrationForm(forms.Form):
    username = forms.CharField(max_length = 20,
                              widget = forms.TextInput(
                                 attrs = {'class' : 'form-control'}))
    email=forms.CharField(max_length=200,
                           widget = forms.EmailInput(
                              attrs = {'class': 'form-control'}))
    password1 = forms.CharField(max_length = 200, 
                                label='Password', 
                                widget = forms.PasswordInput(
                                    attrs = {'class' : 'form-control'}))
    password2 = forms.CharField(max_length = 200, 
                                label='Confirm password',  
                                widget = forms.PasswordInput(
                                    attrs = {'class' : 'form-control'}))


    # Customizes form validation for properties that apply to more
    # than one field.  Overrides the forms.Form.clean function.
    def clean(self):
        # Calls our parent (forms.Form) .clean function, gets a dictionary
        # of cleaned data as a result
        cleaned_data = super(RegistrationForm, self).clean()

        # Confirms that the two password fields match
        password1 = cleaned_data.get('password1')
        password2 = cleaned_data.get('password2')
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords did not match.")

        # Generally return the cleaned data we got from our parent.
        return cleaned_data


    # Customizes form validation for the username field.
    def clean_username(self):
        # Confirms that the username is not already present in the
        # User model database.
        username = self.cleaned_data.get('username')
        if User.objects.filter(username__exact=username):
            raise forms.ValidationError("Username is already taken.")

        # Generally return the cleaned data we got from the cleaned_data
        # dictionary
        return username

    def clean_email(self):
        # Confirms that the email is not already present in the
        # User model database.
        email = self.cleaned_data.get('email')
        if User.objects.filter(email__exact=email):
            raise forms.ValidationError("Email is already taken.")

        # Generally return the cleaned data we got from the cleaned_data
        # dictionary
        return email

class FBRegistrationForm(forms.Form):
    username = forms.CharField(max_length = 20,
                              widget = forms.TextInput(
                                 attrs = {'class' : 'form-control'}))
    password = forms.CharField(max_length = 200, 
                                label='Password', 
                                widget = forms.PasswordInput(
                                    attrs = {'class' : 'form-control'}))

   


    # Customizes form validation for the username field.
    def clean_username(self):
        # Confirms that the username is not already present in the
        # User model database.
        username = self.cleaned_data.get('username')
        if User.objects.filter(username__exact=username):
            raise forms.ValidationError("Username is already taken.")

        # Generally return the cleaned data we got from the cleaned_data
        # dictionary
        return username





class LoginForm(AuthenticationForm):
    username = forms.CharField(widget = forms.TextInput(
        attrs={'class': 'form-control',
               'placeholder' : 'Username',
        }))
    password = forms.CharField(widget = forms.PasswordInput(
        attrs={'class' : 'form-control',
               'placeholder' : 'Password',
        }))

class ProfileForm(forms.ModelForm):
    class Meta:
        model=Personal
        exclude =('user','intro','score','greetings','friends','favorite_singers','favorite_songs','createtime')
        widgets = {'picture':forms.FileInput() }

class EditPasswordForm(forms.Form):
    password1 = forms.CharField(max_length = 200, 
                                label='Password', 
                                widget=forms.PasswordInput(attrs={'class':'input-block-level form-control', 'placeholder':'Enter new password'}))
    password2 = forms.CharField(max_length = 200,
                                label='Confirm Password',
                                widget=forms.PasswordInput(attrs={'class':'input-block-level form-control', 'placeholder':'Confirm Password'})) 
    def clean(self):
        cleaned_data = super(ResetForgotPasswordForm,self).clean()
        password1 = cleaned_data.get('password1')
        password2 = cleaned_data.get('password2')
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError("Passwords did not match.")
        return cleaned_data

class ForumForm(ModelForm):
    class Meta:
        model = Forum
        fields = '__all__'
       
class GreetingForm(ModelForm):
    class Meta:
        model = Greeting
        fields = '__all__'
        

