// TODO: Somehow address repeating code from music-play-search.js
// Get album information from Spotify API
var getAlbum = function(albumId, callback) {
	$.ajax({
		url: 'https://api.spotify.com/v1/albums/' + albumId,
		async: false,
		success: function(response) {
			console.log("Retrieving album image...");
			callback(response);
		}
	});
}

$(document).ready(function() {
	// Adding friend
	$(document).on("click", ".add-friend", function(event) {
		console.log("added friend");
		
		$.ajax({
			type : "POST",
			async : false,
			url : "/profile/friend/add",
			data : {
				friend_id : $(this).attr("data-attr-id")
			}
		});

		// Switch to show button of removing friend
		$(this).get(0).lastChild.nodeValue = " Remove Friend";
		$(this).removeClass("add-friend").addClass("remove-friend");
		$(".now-plus").removeClass("glyphicon glyphicon-plus now-plus")
			.addClass("glyphicon glyphicon-minus now-minus");
	});

	// Removing friend
	$(document).on("click", ".remove-friend", function(event) {
		console.log("removed friend");

		$.ajax({
			type : "POST",
			async : false,
			url : "/profile/friend/remove",
			data : {
				friend_id: $(this).attr("data-attr-id")
			}
		});

		// Switch to show button of adding friend
		$(this).get(0).lastChild.nodeValue = " Add Friend";
		$(this).removeClass("remove-friend").addClass("add-friend");
		$(".now-minus").removeClass("glyphicon glyphicon-minus now-minus")
			.addClass("glyphicon glyphicon-plus now-plus");
	});

	// Sending a greeting
	$(".greeting-form").on("submit", function(event) {
		event.preventDefault();
		console.log("detected");
		var form = $(".greeting-form");
		console.log(form.serialize());
		$.ajax({
			type: "POST",
			url: "/greeting/add",
			data: form.serialize(),
			success: function(response) {
				$(".greeting-form")[0].reset();
			}
		});
	});

	// Get list of favorite singers
	$.ajax({
		type: "POST",
		data : {
			user_id : $("#album-dashboard").attr("data-profile-id")
		},
		url: "/music/favorite/singers",
		success: function(response) {
			$.each(response.favorite_singers, function() {
				var artist = this[0];
				var artistId = this[1];
				$.ajax({
					type: "GET",
					url: 'https://api.spotify.com/v1/artists/'+artistId+'/top-tracks?country=US',
					async: false,
					success: function(response1) {
						var source = $("#artist-template").html();
						var template = Handlebars.compile(source);
						var context = {artist: artist, artistId: artistId, track: response1.tracks} 
						var html = template(context);
						console.log(html);
						$(".singers-list").append(html);
					}
				});	
			});
		}
	});

	// Get list of favorite songs
	$.ajax({
		type: "POST",
		data : {
			user_id : $("#album-dashboard").attr("data-profile-id")
		},
		url: "/music/favorite/songs",
		success: function(response) {
			var rowNum = 0;
			$.each(response.songs, function(i) {
				var songId = this.toString();
				if (i % 2 == 0) {
					rowNum++;
					$("#album-dashboard").append(
						'<div class="row placeholders" id="album-row' + rowNum +
						'"></div>');
				}
				getAlbum(songId, function(data){
					var spotify_url = data.external_urls.spotify;
					var albumName = data.name;
					var albumImg = data.images[1].url;
					var artist = data.artists[0].name;
					var artistId = data.artists[0].id;
					$.ajax({
						type: "POST",
						url: "/get/album/template",
						async: false,
						data : {
							album_num: i,
							album_name: albumName,
							album_img: albumImg,
							album_id: songId,
							album_artist: artist,
							artist_id: artistId,
							col_size : 6,
							spotify_url : spotify_url
						},
						success: function(response) {
							$("#album-row" + rowNum).append(response);
						}
					});
				});
			});
		},
		error: function( xhr, status, errorThrown ) {
			console.log( "Sorry, there was a problem!" );
			console.log( "Error: " + errorThrown );
			console.log( "Status: " + status );
			console.dir( xhr );
		},
		complete: function() {
			// Favoriting song 
			$(document).on("click", ".red-heart", function(event) {
				var albumId = $(this).attr("data-album-id");
				console.log("clicked gray heart");
				getAlbum(albumId, function(data) {
					$.ajax({
						type: "POST",
						url: "/music/favorited/song",
						data: {
							singer : data.artists[0].name,
							singer_id : data.artists[0].id,
							song : data.name,
							song_id : albumId
						},
						crossDomain: true
					});
				});
				$(this).toggleClass("red-heart my-liked");
			});

			// Unfavoriting a song
			$(document).on("click", ".my-liked", function(event) {
				var albumId = $(this).attr("data-album-id");
				console.log("clicked red heart");
				$.ajax({
					type: "POST",
					url: "/music/unfavorited/song",
					data: {
						song_id : albumId
					},										
					crossDomain: true
				});
				$(this).toggleClass("my-liked red-heart");
			});		

			// Following a singer
			$(document).on("click", ".singer-follow", function(event) {
				console.log("Following artist");
				var singer = $(this).attr("data-artist-name");
				var singerId = $(this).attr("data-artist-id");
				$.ajax({
					type: "POST",
					url: "/music/favorited/singer",
					data: {
						singer: singer,
						singer_id: singerId
					},
					crossDomain: true
				});
				$(this).toggleClass("singer-follow singer-unfollow");
				$(this).html('Unfollow '+ singer);
			});

			// Unfollowing a singer
			$(document).on("click", ".singer-unfollow", function(event) {
				console.log("Unfollowing artist");
				var singer = $(this).attr("data-artist-name");
				var singerId = $(this).attr("data-artist-id");
				$.ajax({
					type: "POST",
					url: "/music/unfavorited/singer",
					data: {
						singer_id: singerId
					},
					crossDomain: true
				});
				$(this).toggleClass("singer-unfollow singer-follow");
				$(this).html('Follow '+ singer);
			});

			// Play music 
			var audioObject;
			var prevObject;
			$(".play-btn").click(function(event) {
				if (audioObject != null) {
					audioObject.pause();											
				}
				var albumId = $(this).attr("data-album-id");
				var playObj = $(this);
				prevObject = playObj;
				getAlbum(albumId, function(data) {
					audioObject = new Audio(data.tracks.items[0].preview_url);
					if (playObj.attr("playing") == "false") {
						audioObject.play();
						playObj.attr("playing", "true");
						console.log('click once');
					} else {
						audioObject.pause();
						playObj.attr("playing", "false");
						console.log('clicked again');
					}
				});	
			});

			// Info Tooltip
			$(".info-icon").hover(function(event) {
				var pos = $(this).position();
			    $($(this).data("tooltip")).css({
			        left: pos.left-250,
			        top: pos.top-50,
			        width: "75%",
			    }).stop().show(100);
			}, function() {
			    $($(this).data("tooltip")).hide();
			});
			$(".info-tip").hover(function(event){
				$(this).show();
			}, function() {
				$(this).hide();
			});
		}
	});
});