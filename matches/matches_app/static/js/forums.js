// Get list of forums
$(document).ready(function() {
	$.ajax({
		type: "POST",
		url: "/forums/list",
		dataType: "html",
		success: function(response) {
			$("ul.forums").append(response);
		}
	});

	// Query advanced search based on tags, title, etc...
	$(".adv-search").on("submit", function(event) {
		event.preventDefault();
		var form = $(this);
		$.ajax({
			type: "POST",
			url: "/forum/search",
			data: form.serialize(),
			success: function(response) {
				$("ul.forums").html("");
				$("ul.forums").append(response);
				$("#advancedSearch").modal("hide");
				form[0].reset();
			}
		});
	});

	// Add new comment
	$(document).on("submit", ".new-comment", function(event) {
		event.preventDefault();
		var form = $(".new-comment")
		$.ajax({
			type: "POST",
			url: "/forum/comment/add",
			data : form.serialize(),
			success: function(response) {
				form[0].reset();
				$(".comments-list").prepend(response);
			}
		});
	});

	// Add new response
	$(document).on("submit", ".reply-form", function(event){
		event.preventDefault();
		var form = $(this);
		console.log(form.serialize());
		$.ajax({
			type: "POST",
			url: "/forum/comment/respond",
			data: form.serialize(),
			success: function(response) {
				form[0].reset();
				$(".comments-list").prepend(response);
			}
		});
	});

	// Get list of tags when new forum topic made
 	var tags = $('#my-tag-list').tags({
    	tagSize: "lg",
		suggestions:["music", "recent"],
    });
   
   	// Create new forum topic
   	$(document).on("submit", ".topic", function(event) {
   		event.preventDefault();
   		console.log(tags.getTags());
   		var formData = $(".topic").serializeArray();
   		console.log(formData);
   		formData.push({name: 'tags', value: tags.getTags()});
   		$.ajax({
   			type: "POST",
   			url: "/forums/done",
   			data: formData,
   			success: function(response) {
   				var href = window.location.href;
				window.location = href.replace(/forums\/new\/topic/, "forums");
   			}
   		});
   	});


});