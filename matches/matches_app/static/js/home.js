$(document).ready(function () {
	// Get authorization from Spotify API
	$.ajax({
		type: "GET",
		url: '/spotify/authorize',
		success: function(response) {
			// Get recent releases from Spotify API
			$.ajax({
				type: "GET",
				url: "https://api.spotify.com/v1/browse/new-releases",
				data: {
					limit: 10,
					country: "US"
				},
				headers: {
					'Authorization' : 'Bearer ' + response.access_token 
				},
				success: function(response) {
					$.each(response.albums.items, function(i) {
						if(i == 0) {
							$(".carousel-indicators").append('<li data-target="#myCarousel" data-slide-to="' + i +
							'" class="active indicator"></li>');
						}
						else {
							$(".carousel-indicators").append('<li data-target="#myCarousel" data-slide-to="' + i +
							'" class="indicator"></li>');
						}
						$.ajax({
							type: "GET",
							url: this.href,
							success: function(response) {
								var artist = response.artists[0].name;
								var album = response.name;
								var albumImg = response.images[1].url;
								if(i == 0) {
									codeBlock = '<div class="item active">';
								}
								else {
									codeBlock = '<div class="item">';
								}
								codeBlock += '<h3> ' + artist + ' - ' + album + '</h3>' +
									'<img src="' + albumImg + '" alt="..." width="350" height="350"/></div>'; 
								$(".carousel-inner").append(codeBlock);
							}
						});
					});
				}
			});
		}
	});

	// Get recent favorite songs from backedn
	$.ajax({
		type: "POST",
		url: "/music/recent/favorites",
		success: function(response) {	
			$.each(response, function() {
				console.log(this[0]);
				var username = this[1];
				var song_id = this[2];
				$.ajax({
					type: "GET",
					url: "https://api.spotify.com/v1/albums/" + song_id,
					success: function(response) {
						var img_src = response.images[1].url;
						var artist = response.artists[0].name;
						var album = response.name;
						// Get the template html
						$.ajax({
							type: "POST",
							url: "/music/recent/template",
							data: {
								img_src : img_src,
								username : username,
								artist : artist,
								album : album
							},
							success: function(response) {
								$(".recent-posts").append(response);
							}
						});
					}
				});
			});
		}
	});
});