# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('matches_app', '0003_auto_20141129_1920'),
    ]

    operations = [
        migrations.AlterField(
            model_name='personal',
            name='picture',
            field=models.ImageField(default=b'/media/default.jpg', upload_to=b'', blank=True),
        ),
    ]
