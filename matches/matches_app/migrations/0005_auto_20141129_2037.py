# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('matches_app', '0004_auto_20141129_1938'),
    ]

    operations = [
        migrations.AlterField(
            model_name='personal',
            name='greetings',
            field=models.ManyToManyField(to=b'matches_app.Greeting', null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='personal',
            name='location',
            field=models.CharField(max_length=42, blank=True),
        ),
        migrations.AlterField(
            model_name='personal',
            name='picture',
            field=models.ImageField(default=b'/media/default.jpg', upload_to=b'profile_pics', blank=True),
        ),
    ]
