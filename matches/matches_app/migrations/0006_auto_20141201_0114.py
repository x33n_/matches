# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('matches_app', '0005_auto_20141129_2037'),
    ]

    operations = [
        migrations.AlterField(
            model_name='forum',
            name='comments',
            field=models.ManyToManyField(to=b'matches_app.Comment', null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='forum',
            name='tags',
            field=models.ManyToManyField(to=b'matches_app.Tag', null=True, blank=True),
        ),
    ]
