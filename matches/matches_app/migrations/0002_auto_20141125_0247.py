# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('matches_app', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='comment',
            name='replied_comment',
            field=models.ForeignKey(related_name=b'test', blank=True, to='matches_app.Comment', null=True),
        ),
    ]
