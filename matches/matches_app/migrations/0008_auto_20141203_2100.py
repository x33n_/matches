# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('matches_app', '0007_auto_20141201_2012'),
    ]

    operations = [
        migrations.AddField(
            model_name='personal',
            name='score',
            field=models.IntegerField(default=0, max_length=8),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='personal',
            name='favorite_songs',
            field=models.ManyToManyField(related_name=b'favoritesong', to=b'matches_app.Song'),
        ),
        migrations.AlterField(
            model_name='personal',
            name='picture',
            field=models.ImageField(default=b'/media/profile_pics/default.jpg', upload_to=b'profile_pics', blank=True),
        ),
    ]
